CREATE TABLE person_model
(
    id   TEXT PRIMARY KEY,
    name TEXT NOT NULL
)