print('Start USER-INIT #################################################################');

db = db.getSiblingDB('admin');
db.createUser({
    user: "file-service",
    pwd: "SuperStrong12345",
    roles: [
        {
            role: "readWrite",
            db: "file"
        },
    ]
})

print('END USER-INIT #################################################################');